saleskuPluginProduct.prototype.setComparePrice = function (compare_price) {
    if (compare_price) {
        $compare_price = this.getElements().ComparePrice();
        if (!$compare_price.length) {
            $compare_price = $(this.getElements().Selectors().compare_price_html);
            this.getElements().Price().after($compare_price);
        }
        $compare_price.html('<span class="compare-inner">'+this.currencyFormat(compare_price,true)+'</span>').show();
    } else {
        this.getElements().ComparePrice().remove();
    }
};